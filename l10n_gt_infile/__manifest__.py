{
    "name": "L10N GT INFILE",
    "version": "14.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "OPL-1",
    "depends": [
        "l10n_gt_edi",
    ],
    "external_dependencies": {
        "python": [
            "gt_sat_infile_api",
        ],
    },
    "data": [
        # security
        # data
        # reports
        # views
        "views/account_move.xml",
        "views/res_company.xml",
    ],
}
