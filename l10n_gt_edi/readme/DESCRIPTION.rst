==========
English 🇺🇸
==========
This modules enables the electronic invoice using EDI for Guatemala.
The main public is Guatemala so the rest of the documentation is in spanish.

==========
Spanish 🇲🇽
==========
Este módulo permite la facturación electrónica para Guatelmala utilizando EDI.
Utiliza una libreríá exteran `gt-sat-api` para la generación del XML.
